//
//  WebViewController.swift
//  product
//
//  Created by PREMKUMAR on 30/09/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit
import WebKit

class WebViewController: GSBaseViewController, WKUIDelegate, WKNavigationDelegate {
    
    @IBOutlet weak var activityIndicator : UIActivityIndicatorView!
    @IBOutlet weak var webView: WKWebView!
    
    var webPage = Webpage("Greenwave Systems","https://greenwavesystems.com")
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.title = webPage.title
        let webURL = webPage.url
        let loadWebURL = URL(string: webURL)
        
        if Reachability.isInternetAvailable() {
            let webRequest = URLRequest(url: loadWebURL!)
            webView.uiDelegate = self
            webView.navigationDelegate = self
            webView.load(webRequest)
            activityIndicator.startAnimating()
        }
        else {
            activityIndicator.stopAnimating()
            
            self.showAlert("Greenwave Systems", "Please connect to internet", "Dismiss")
        }
    
    }
    
    func webView(_ webView: WKWebView, didFinish navigation: WKNavigation!) {
        activityIndicator.stopAnimating()
    }
    
    func webView(_ webView: WKWebView, didFail navigation: WKNavigation!, withError error: Error) {
        activityIndicator.stopAnimating()
    }
}

