//
//  NewsViewController.swift
//  product
//
//  Created by PREMKUMAR on 28/09/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class NewsViewController: GSBaseViewController {

    /*var newsList = [Webpage("BLOG", "https://greenwavesystems.com/news/blog/"),
                        Webpage("EVENTS", "https://greenwavesystems.com/news/events/"),
                        Webpage("IN THE NEWS", "https://greenwavesystems.com/news/news/"),
                        Webpage("PRESS KIT", "https://greenwavesystems.com/news/press-kit/"),
                        Webpage("PRESS RELEASES", "https://greenwavesystems.com/category/press-releases/"),
                        Webpage("VIDEOS", "https://greenwavesystems.com/news/videos/")
                        ]*/
    
    // Webservice data
    var newsList = [Webpage]()
    let newsViewModel = NewsViewModel()
    
    lazy var newsListViewController: ListViewController = { [unowned self] in
        return self.childViewControllers[0] as! ListViewController
        }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.fetchDataFroWebservice()
    }
    
    func fetchDataFroWebservice() {
        
        if Reachability.isInternetAvailable() {
            
            self.displayNavBarActivity()
            
            newsViewModel.fetchNews() { [weak self] news in
                self?.dismissNavBarActivity()
                guard let newsArray = news else { return }
                
                self?.newsList = newsArray
                self?.newsListViewController.refreshTableView(with:newsArray)
            }
        }
        else {
            self.showAlert("Greenwave Systems", "Please connect to internet", "Dismiss")
        }
    }
    
    override func fetchData() {
        self.fetchDataFroWebservice()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let lsitVC = segue.destination as? ListViewController, segue.identifier == "showNewsList" {
            lsitVC.list = newsList
        }
    }

}
