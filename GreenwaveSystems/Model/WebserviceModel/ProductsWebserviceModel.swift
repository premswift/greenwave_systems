//
//  ProductsWebserviceModel.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

struct ProductsWebserviceModel: Codable {
    
    let products: [Products]
    
    struct Products : Codable {
        let title: String
        let url: String
    }
}
