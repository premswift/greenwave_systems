//
//  AboutViewModel.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class AboutViewModel: NSObject {

    let fetchAboutURL = "http://mizhtechnology.com/GreenwaveSystems/Webservice/About.php?key=GreenwaveSystemsPlusPrem"
    
    func fetchAbout(_ completion: @escaping ([Webpage]?)->()){
        WebserviceController.callWebserviceAndFetchData(AboutWebserviceModel.self, fetchAboutURL) { (results) in
            
            var aboutList = [Webpage]()
            
            switch results {
            case .Success(let value):
                _ = value.about.map { webserviceAbout in
                    let about = Webpage(webserviceAbout.title, webserviceAbout.url)
                    aboutList.append(about)
                }
            case .Failure(let error):
                print(error.localizedDescription)
            }
            
            DispatchQueue.main.async {
                completion(aboutList)
            }
        }
    }
    
}
