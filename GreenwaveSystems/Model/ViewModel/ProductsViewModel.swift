//
//  ProductsViewModel.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class ProductsViewModel: NSObject {

    let fetchProductsURL = "http://mizhtechnology.com/GreenwaveSystems/Webservice/Products.php?key=GreenwaveSystemsPlusPrem"
    
    func fetchProducts(_ completion: @escaping ([Webpage]?)->()){
        WebserviceController.callWebserviceAndFetchData(ProductsWebserviceModel.self, fetchProductsURL) { (results) in
            
            var products = [Webpage]()
            
            switch results {
            case .Success(let value):
                _ = value.products.map { webserviceProduct in
                    let product = Webpage(webserviceProduct.title, webserviceProduct.url)
                    products.append(product)
                }
            case .Failure(let error):
                print(error.localizedDescription)
            }
            
            DispatchQueue.main.async {
                completion(products)
            }
        }
    }
}

