//
//  WebserviceController.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit


enum ResultType<T> {
    case Success(T)
    case Failure(e: Error)
}

// Error for unknown case
enum JSONDecodingError: Error, LocalizedError {
    case unknownError
    
    public var errorDescription: String? {
        switch self {
        case .unknownError:
            return NSLocalizedString("Unknown Error occured", comment: "")
        }
    }
}

class WebserviceController: NSObject {

    
    // Get News source from /sources endpoint of NewsAPI
    class func callWebserviceAndFetchData<T> (_ classType: T.Type, _ url: String, _ completion: @escaping (ResultType<T>) -> ()) where T:Codable {
        
        let sourceURL = URL(string: url)!
        
        let baseUrlRequest = URLRequest(url: sourceURL)
        let session = URLSession.shared
        
        session.dataTask(with: baseUrlRequest, completionHandler: { (data, response, error) in
            guard error == nil else {
                completion(ResultType.Failure(e: error!))
                return
            }
            
            guard let data = data else {
                completion(ResultType.Failure(e: error!))
                return
            }
            
            do {
                let jsonFromData =  try JSONDecoder().decode(classType, from: data)
                completion(ResultType.Success(jsonFromData))
            } catch DecodingError.dataCorrupted(let context) {
                completion(ResultType.Failure(e: DecodingError.dataCorrupted(context)))
            } catch DecodingError.keyNotFound(let key, let context) {
                completion(ResultType.Failure(e: DecodingError.keyNotFound(key, context)))
            } catch DecodingError.typeMismatch(let type, let context) {
                completion(ResultType.Failure(e: DecodingError.typeMismatch(type, context)))
            } catch DecodingError.valueNotFound(let value, let context) {
                completion(ResultType.Failure(e: DecodingError.valueNotFound(value, context)))
            } catch {
                completion(ResultType.Failure(e:JSONDecodingError.unknownError))
            }
            
        }).resume()
    }
    
}



