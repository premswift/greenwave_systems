//
//  ProductsViewController.swift
//  product
//
//  Created by PREMKUMAR on 28/09/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class ProductsViewController: GSBaseViewController {

    // Local Data
    /*var productsList = [Webpage("FOR IOT", "https://greenwavesystems.com/product/axon-iot/"),
                        Webpage("FOR NETWORKING", "https://greenwavesystems.com/product/axon-networking/"),
                        Webpage("FOR MEDIA", "https://greenwavesystems.com/product/axon-media/"),
                        Webpage("FOR MOBILE", "https://greenwavesystems.com/product/axon-for-mobile-iot/")
                        ]*/
    
    // Webservice data
    var productsList = [Webpage]()
    let productsViewModel = ProductsViewModel()
    
    lazy var productsListViewController: ListViewController = { [unowned self] in
        return self.childViewControllers[0] as! ListViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        self.fetchDataFroWebservice()
    }

    func fetchDataFroWebservice() {
        
        if Reachability.isInternetAvailable() {
            
            self.displayNavBarActivity()
            
            productsViewModel.fetchProducts() { [weak self] products in
                self?.dismissNavBarActivity()
                guard let productsArray = products else { return }
                
                self?.productsList = productsArray
                self?.productsListViewController.refreshTableView(with:productsArray)
            }
        }
        else {
            self.showAlert("Greenwave Systems", "Please connect to internet", "Dismiss")
        }
    }
    
    override func fetchData() {
        self.fetchDataFroWebservice()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let lsitVC = segue.destination as? ListViewController, segue.identifier == "showProductsList" {
            lsitVC.list = productsList
        }
    }
}
