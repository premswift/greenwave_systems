//
//  Webpage.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class Webpage: NSObject {

    var title = ""
    var url = ""
    
    init(_ title: String, _ url: String) {
        self.title = title
        self.url = url
    }
}
