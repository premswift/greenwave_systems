//
//  NewsViewModel.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class NewsViewModel: NSObject {

    let fetchNewsURL = "http://mizhtechnology.com/GreenwaveSystems/Webservice/News.php?key=GreenwaveSystemsPlusPrem"
    
    func fetchNews(_ completion: @escaping ([Webpage]?)->()){
        WebserviceController.callWebserviceAndFetchData(NewsWebserviceModel.self, fetchNewsURL) { (results) in
            
            var newsList = [Webpage]()
            
            switch results {
            case .Success(let value):
                _ = value.news.map { webserviceNews in
                    let news = Webpage(webserviceNews.title, webserviceNews.url)
                    newsList.append(news)
                }
            case .Failure(let error):
                print(error.localizedDescription)
            }
            
            DispatchQueue.main.async {
                completion(newsList)
            }
        }
    }
}
