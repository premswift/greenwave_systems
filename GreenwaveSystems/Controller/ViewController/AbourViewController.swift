//
//  AbourViewController.swift
//  product
//
//  Created by PREMKUMAR on 28/09/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class AbourViewController: GSBaseViewController {

    // Local data
    /*var aboutList = [Webpage("ABOUT US", "https://greenwavesystems.com/about-greenwave-systems/"),
                    Webpage("LEADERSHIP", "https://greenwavesystems.com/about-greenwave-systems/management/"),
                    Webpage("BOARD OF DIRECTORS", "https://greenwavesystems.com/about-greenwave-systems/board-of-directors/"),
                    Webpage("CAREERS", "https://greenwavesystems.com/about-greenwave-systems/greenwave-careers/"),
    ]*/
    
    // Webservice data
    var aboutList = [Webpage]()
    let aboutViewModel = AboutViewModel()
    
    lazy var aboutListViewController: ListViewController = { [unowned self] in
        return self.childViewControllers[0] as! ListViewController
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Do any additional setup after loading the view.
        
        self.fetchDataFroWebservice()
    }
    
    func fetchDataFroWebservice() {
        
        if Reachability.isInternetAvailable() {
            self.displayNavBarActivity()
            
            aboutViewModel.fetchAbout() { [weak self] about in
                self?.dismissNavBarActivity()
                guard let aboutArray = about else { return }
                
                self?.aboutList = aboutArray
                self?.aboutListViewController.refreshTableView(with:aboutArray)
            }
        }
        else {
            self.showAlert("Greenwave Systems", "Please connect to internet", "Dismiss")
        }
    }
    
    override func fetchData() {
        self.fetchDataFroWebservice()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let lsitVC = segue.destination as? ListViewController, segue.identifier == "showAboutList" {
            lsitVC.list = aboutList
        }
    }
    
}
