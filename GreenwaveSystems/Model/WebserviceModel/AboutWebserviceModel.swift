//
//  AboutWebserviceModel.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

struct AboutWebserviceModel: Codable {
    
    let about: [About]
    
    struct About : Codable {
        let title: String
        let url: String
    }
}
