//
//  GSBaseViewController.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class GSBaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func displayNavBarActivity() {
        self.navigationItem.rightBarButtonItem = nil
        self.createIndicatorIcon()
    }
    
    func dismissNavBarActivity() {
        DispatchQueue.main.asyncAfter(deadline: .now() + 0.1) {
            self.navigationItem.rightBarButtonItem = nil
            self.createRefreshIcon()
        }
        
    }
    
    func createIndicatorIcon(){
        let indicator = UIActivityIndicatorView(activityIndicatorStyle: .white)
        indicator.startAnimating()
        let item = UIBarButtonItem(customView: indicator)
        self.navigationItem.rightBarButtonItem = item
    }
    
    func createRefreshIcon(){
        // Create  button for navigation item with refresh
        let refreshButton =  UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.refresh, target: self, action: #selector(GSBaseViewController.fetchData))
        // I set refreshButton for the navigation item
        self.navigationItem.rightBarButtonItem = refreshButton
    }
    
    @objc func fetchData() {
        print("will be handled from child")
    }
}

extension GSBaseViewController {
    
    func showAlert(_ title: String, _ message: String, _ actionButtonTitle: String) {
        let alertController = UIAlertController(title: title, message:message
            , preferredStyle: UIAlertControllerStyle.alert)
        alertController.addAction(UIAlertAction(title: actionButtonTitle, style: UIAlertActionStyle.default,handler: nil))
        
        self.present(alertController, animated: true, completion: nil)
    }
}

