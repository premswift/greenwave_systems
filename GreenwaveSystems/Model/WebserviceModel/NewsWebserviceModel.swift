//
//  NewsWebserviceModel.swift
//  GreenwaveSystems
//
//  Created by PREMKUMAR on 01/10/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

struct NewsWebserviceModel: Codable {
    
    let news: [News]
    
    struct News : Codable {
        let title: String
        let url: String
    }
}
