//
//  ContactViewController.swift
//  product
//
//  Created by PREMKUMAR on 28/09/17.
//  Copyright © 2017 PREMKUMAR. All rights reserved.
//

import UIKit

class ContactViewController: UIViewController {

    let contactWebpage = Webpage("CONTACT","https://greenwavesystems.com/contact-greenwave-systems/")
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        
        if let webVC = segue.destination as? WebViewController {
            webVC.webPage = contactWebpage
        }
    }

}
